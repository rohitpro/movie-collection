package com.thomaskioko.livedatademo.utils;


import android.graphics.Color;

public class AppConstants {
    /**
     * Connection timeout duration
     */
    public static final int CONNECT_TIMEOUT = 60 * 1000;
    /**
     * Connection Read timeout duration
     */
    public static final int READ_TIMEOUT = 60 * 1000;

    public static final int WRITE_TIMEOUT = 60 * 1000;

    public static final String BASE_URL = "https://api.themoviedb.org/3/";


    static boolean DEBUG = true;

    // the dimens unit is dp or sp, not px
    static final float DEFAULT_LINE_MARGIN = 5;
    static final float DEFAULT_TAG_MARGIN = 5;
    static final float DEFAULT_TAG_TEXT_PADDING_LEFT = 8;
    static final float DEFAULT_TAG_TEXT_PADDING_TOP = 5;
    static final float DEFAULT_TAG_TEXT_PADDING_RIGHT = 8;
    static final float DEFAULT_TAG_TEXT_PADDING_BOTTOM = 5;
    static final float LAYOUT_WIDTH_OFFSET = 2;

    static final float DEFAULT_TAG_TEXT_SIZE = 14f;
    static final float DEFAULT_TAG_DELETE_INDICATOR_SIZE = 14f;
    static final float DEFAULT_TAG_LAYOUT_BORDER_SIZE = 0f;
    static final float DEFAULT_TAG_RADIUS = 100;
    static final int DEFAULT_TAG_LAYOUT_COLOR = Color.parseColor("#00BFFF");
    static final int DEFAULT_TAG_LAYOUT_COLOR_PRESS = Color.parseColor("#88363636");
    static final int DEFAULT_TAG_TEXT_COLOR = Color.parseColor("#ffffff");
    static final int DEFAULT_TAG_DELETE_INDICATOR_COLOR = Color.parseColor("#ffffff");
    static final int DEFAULT_TAG_LAYOUT_BORDER_COLOR = Color.parseColor("#ffffff");
    static final String DEFAULT_TAG_DELETE_ICON = "×";
    public static final boolean DEFAULT_TAG_IS_DELETABLE = false;
}
